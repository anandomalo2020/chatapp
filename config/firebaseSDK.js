import firebase from 'firebase';

class FirebaseSDK {
    uid = '';
    messagesRef = null;
  constructor() {
    if (!firebase.apps.length) {
      //avoid re-initializing
      firebase.initializeApp({
        apiKey: "AIzaSyCAcmfms6fDabtTDllG8XYCLv99HYWY0ew",
         authDomain: "chatapp-27dab.firebaseapp.com",
         databaseURL: "https://chatapp-27dab.firebaseio.com",
         projectId: "chatapp-27dab",
         storageBucket: "chatapp-27dab.appspot.com",
         messagingSenderId: "868777582711",
         appId: "1:868777582711:web:d5320421c1e777fdaba672",
         measurementId: "G-Z9LFEX5JHX"
      });
    }
//     firebase.auth().onAuthStateChanged((user) => {
//          if (user) {
//            console.log("user data == ",user)
//            //this.setUid(user.uid);
//          } else {
//            firebase.auth().signInAnonymously().catch((error) => {
//              alert(error.message);
//            });
//          }
//        });
  }

  setUid(value) {
      this.uid = value;
    }
    getUid() {
      return this.uid;
    }

  login = async (user, success_callback, failed_callback) => {
    await firebase
      .auth()
      .signInWithEmailAndPassword(user.email, user.password)
      .then(success_callback, failed_callback);
     var userf = firebase.auth().currentUser;
     this.setUid(userf.uid);
      console.log("after login user data",userf)
  };
  createAccount = async user => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(user.email, user.password)
      .then(
        function() {
          console.log(
            'created user successfully. User email:' +
              user.email +
              ' name:' +
              user.name
          );
          var userf = firebase.auth().currentUser;
          userf.updateProfile({ displayName: user.name }).then(
            function() {
              console.log('Updated displayName successfully. name:' + user.name);
              alert(
                'User ' + user.name + ' was created successfully. Please login.'
              );
            },
            function(error) {
              console.warn('Error update displayName.');
            }
          );
        },
        function(error) {
          console.error('got error:' + typeof error + ' string:' + error.message);
          alert('Create account failed. Error: ' + error.message);
        }
      );
  };

  uploadImage = async uri => {
    console.log('got image to upload. uri:' + uri);
    try {
      const response = await fetch(uri);
      const blob = await response.blob();
      const ref = firebase
        .storage()
        .ref('avatar')
        .child(uuid.v4());
      const task = ref.put(blob);

      return new Promise((resolve, reject) => {
        task.on(
          'state_changed',
          () => {

          },
          'reject',
          () => resolve(task.snapshot.downloadURL)
        );
      });
    } catch (err) {
      console.log('uploadImage try/catch error: ' + err.message);
    }
  };

  updateAvatar = url => {

    var userf = firebase.auth().currentUser;
    if (userf != null) {
      userf.updateProfile({ avatar: url }).then(
        function() {
          console.log('Updated avatar successfully. url:' + url);
          alert('Avatar image is saved successfully.');
        },
        function(error) {
          console.warn('Error update avatar.');
          alert('Error update avatar. Error:' + error.message);
        }
      );
    } else {
      console.log("can't update avatar, user is not login.");
      alert('Unable to update avatar. You must login first.');
    }
  };

    // retrieve the messages from the Backend
    loadMessages(callback) {
      this.messagesRef = firebase.database().ref('messages');
      this.messagesRef.off();
      const onReceive = (data) => {
        const message = data.val();
        callback({
          _id: data.key,
          text: message.text,
          createdAt: new Date(message.createdAt),
          user: {
           _id: message.user._id,
           name: message.user.name,
          },
        });
      };
      this.messagesRef.limitToLast(20).on('child_added', onReceive);
    }

     // send the message to the Backend
      sendMessage(message) {
        for (let i = 0; i < message.length; i++) {
          this.messagesRef.push({
            text: message[i].text,
            user: message[i].user,
            createdAt: firebase.database.ServerValue.TIMESTAMP,
          });
        }
       }

         // close the connection to the Backend
         closeChat() {
           if (this.messagesRef) {
             this.messagesRef.off();
           }
          }
}
const firebaseSDK = new FirebaseSDK();
export default firebaseSDK;