import { GiftedChat } from 'react-native-gifted-chat'; // 0.3.0
import firebaseSDK from '../config/firebaseSDK';
import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { Text,View,StyleSheet } from 'react-native';
import { YellowBox } from 'react-native';
import _ from 'lodash';

export default class Chat extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: (navigation.state.params || {}).name || 'Chat!'
  });
    user={};
  state = {
    messages: []
  };

  getuser() {
    return {
      name: this.props.route.params.name,
      email: this.props.route.params.email,
      avatar: this.props.route.params.avatar,
      id: firebaseSDK.uid,
      _id: firebaseSDK.uid
    };
  }

  render() {
    return (
    <View style={styles.container}>

          <GiftedChat
                  messages={this.state.messages}
                 onSend={(message) => {
                           firebaseSDK.sendMessage(message);
                         }}
                  user={this.user}
                />
          <StatusBar style="auto" />
        </View>

    );
  }

    componentDidMount() {
    this.user = this.getuser();
    console.log("user data on mount",this.user)
              firebaseSDK.loadMessages(message =>
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, message)
        }))
      );
    }
    componentWillUnmount() {
        firebaseSDK.closeChat();
      console.log("ref",firebaseSDK)
      console.log("props",this.props.route.params)
    }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },
});

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

//export default Chat;